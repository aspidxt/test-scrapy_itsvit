####Summary
This is a simple self-contained script to collect blog posts summary into local storage.
required `scrapy`:
```bash
$ pip install scrapy
``` 

####Key features
* Crawling process is recursive, it start from a first page and move to previous if such exists.
Process will continue until it reach pagination limit (2 pages) or find article that already exists in local storage
* Local storage will be created at 'storage' folder, near the spider-script
* Script can be launched with `show` parameter to display all stored articles keys (full article url is used as key)
* Script can be launched with `show <key>` parameter to display article details by key

Example usage:
```bash
$ python spider_itsvit_blog.py
$ python spider_itsvit_blog.py show https://itsvit.com/blog/will-elastic-cluster-hacked/
```