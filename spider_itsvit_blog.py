#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import atexit
import json
import dbm.dumb
from pprint import pprint

from scrapy import Spider, Item, Field, Request
from scrapy.crawler import CrawlerProcess
from scrapy.http.response.html import HtmlResponse


PARSE_PREVIOUS_PAGES_COUNT = 2

PATH_NEXT_PAGE = "//ol[@class='wp-paginate']/li/a[@class='next']/@href"
PATH_ARTICLES = "//div[@class='blog-wrap']/div[@class='blog-item']"
PATH_ARTICLES_FULL_URL = "p/a[text()='Read more']/@href"
PATH_ARTICLES_TITLE = "a[@class='blog-header-item']/h2/text()"
PATH_ARTICLES_DATE = "div[@class='blog-status']/ul/li/i[@class='fa fa-calendar-o']/../text()"
PATH_ARTICLES_IMAGE = "a/img/@src"
PATH_ARTICLES_TEXT = "p/text()"


class Storage:
    _db_conn = None

    @classmethod
    def _database(cls):
        path = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(path, 'storage')
        if not os.path.exists(path):
            os.makedirs(path)
        if not cls._db_conn:
            cls._db_conn = dbm.dumb.open(os.path.join(path, 'db'))
        return cls._db_conn

    @classmethod
    def close_connection(cls):
        if cls._db_conn:
            cls._db_conn.close()

    @classmethod
    def append_if_not_exist(cls, key, item):
        cls._database().setdefault(key, item)

    @classmethod
    def get_record(cls, key):
        return cls._database().get(key, None)

    @classmethod
    def get_records_keys(cls):
        return cls._database().keys()


class BlogArticle(Item):
    title = Field()
    date = Field()
    image_url = Field()
    short_text = Field()
    full_article_url = Field()


class BlogSpider(Spider):
    name = 'blog'
    allowed_domains = ['itsvit.com']
    start_urls = ['http://itsvit.com/blog']
    parse_extra_pages = PARSE_PREVIOUS_PAGES_COUNT

    def parse(self, response: HtmlResponse):
        response_contain_stored_article = False
        articles = response.xpath(PATH_ARTICLES)
        for article in articles:
            key = article.xpath(PATH_ARTICLES_FULL_URL).extract_first()
            record = BlogArticle()
            record['title'] = article.xpath(PATH_ARTICLES_TITLE).extract_first()
            record['date'] = article.xpath(PATH_ARTICLES_DATE).extract_first()
            record['image_url'] = article.xpath(PATH_ARTICLES_IMAGE).extract_first()
            record['short_text'] = article.xpath(PATH_ARTICLES_TEXT).extract_first()
            record['full_article_url'] = key
            response_contain_stored_article = True if Storage.get_record(key) else response_contain_stored_article
            Storage.append_if_not_exist(key, json.dumps(dict(record)))

        if response_contain_stored_article:
            return

        next_page = response.xpath(PATH_NEXT_PAGE).extract_first()
        if next_page and self.parse_extra_pages > 0:
            self.parse_extra_pages -= 1
            yield Request(response.urljoin(next_page), callback=self.parse)


if __name__ == '__main__':
    action, param = None, None
    try:
        action = sys.argv[1]
        param = sys.argv[2]
    except IndexError:
        pass

    if not action:
        process = CrawlerProcess({'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'})
        process.crawl(BlogSpider)
        process.start()
    elif action == 'show' and not param:
        for i in Storage.get_records_keys():
            print('  ', bytes.decode(i))
    elif action == 'show' and param:
        r = Storage.get_record(param)
        if r:
            pprint(json.loads(bytes.decode(r)))
        else:
            print('record not found')

    atexit.register(Storage.close_connection)
